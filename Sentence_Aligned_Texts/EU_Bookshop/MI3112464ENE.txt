Road transport delivers the goods we need and ensures our personal mobility .
It is one of the most dynamic sectors of the EU economy , creating jobs and prosperity .
Road transport carries goods and people over long and short distances , rapidly , flexibly and cheaply .
It is facing a series of challenges concerning congestion , safety and pollution .
This brochure identifies these challenges and points to key solutions proposed by the European Commission and implemented at the EU level .
Europe Direct is a service to help you find answers to your questions about the European Union .
Freephone number ( * ) :
00 800 6 7 8 9 10 11 ( * ) Certain mobile telephone operators do not allow access to 00 800 numbers or these calls may be billed .
More information on the European Union is available on the Internet ( http : / / europa.eu ) .
Luxembourg : Publications Office of the European Union , 2012
ISBN 978-92-79-22827-8 ( paperback )
ISBN 978-92-78-40969-2 ( EPUB )
doi : 10.2832 / 77741
© European Union , 2012
Reproduction is authorised provided the source is acknowledged .
Illustrations : Laurent Durieux
Photo in foreword : © European Union
Foreword
There is no single market without integrated transport networks .
The principle of a common transport policy was set out in the founding treaties of the European Union ( EU ) .
But its concrete implementation only started in 1992 when the transport market was gradually liberalised and framework rules for access to the profession of road transport operator and for road safety measures were established for all Member States .
Today , road transport is the victim of its own success and faces considerable challenges .
The March 2011 White Paper on transport defined them : ensure mobility on ever more congested road networks , significantly further reduce road fatalities , lower CO2 and other emissions of pollutants from road transport to preserve the environment and lessen the impact of climate change on future generations , and decrease fossil fuel use to improve the Europe 's fuel security .
With the strategy established by the European Commission for the next decade , we can address these challenges and bring about the transformation of transport policy .
During my term as Commission Vice-President responsible for transport , I am fully committed to putting forward a series of key actions for the sector : improving the efficiency of road freight transport by a more integrated internal market and a better integration with other transport modes ; developing infrastructure to guarantee the mobility of persons and goods thanks to a wide trans-European transport network ( TEN-T ) co-financed through the Connecting Europe Facility ; and finally , promoting innovative transport services and deploying intelligent transport systems .
All these actions take into account the economic , social and territorial dimension of the EU .
Road transport is a tool for reviving growth through the enhancement of competitiveness and the creation of jobs .
Together , we will produce a more competitive and resource-efficient transport system , contributing to the implementation of the Europe 2020 strategy for smart , sustainable and inclusive growth .
Siim Kallas
Vice-President of the European Commission
1 . The need for change
Road transport is part of the lifeblood of the European economy and single market .
It delivers goods across Europe fast , efficiently , flexibly and cheaply .
About 44 % of goods transported in the EU go by road .
People also travel mainly by road , with private cars accounting for 73 % of passenger traffic .
Road transport is a vital economic sector in its own right , employing about 5 million people across the EU and generating close to 2 % of its GDP .
It is one of the most dynamic sectors of the European economy , with small firms operating alongside big international transport groups .
It handles freight and passengers over short and long hauls .
Because of its flexibility , road transport links all regions of the EU to each other and to Europe 's principal networks for goods and passenger traffic .
The EU 's objective is to create the conditions whereby the road transport sector can operate efficiently , safely and with a minimum impact on our environment .
The challenges
Today , the road transport sector in the EU is facing a number of challenges .
Drivers are confronted with ever more congested roads while one out of four heavy goods vehicles still runs empty .
Road users expect safer and more secure roads and the working conditions of professional drivers should remain attractive .
At the same time , fuel prices keep on rising , as does the need to reduce air and noise pollution and the carbon footprint to which road transport contributes .
Congestion is not just a nuisance for road users ; it also results in an enormous waste of fuel and productivity .
Many manufacturing processes depend on just-in-time deliveries and free flow transport for efficient production .
Congestion costs the EU economy more than 1 % of GDP – in other words , more than the EU budget .
To reduce it , the EU needs more efficient transport and logistics , better infrastructure and the ability to optimise capacity use .
Road safety , secure rest areas and decent working conditions for drivers are also of paramount importance .
Even if a lot of progress has been made in the last 10 years to improve road safety , our society cannot tolerate road fatalities .
In addition , Europe may well face a shortage of professional drivers in the coming years .
More efforts must be made to attract skilled and well-trained drivers , in particular among young people .
Last but not least , Europe needs transport which is cleaner and less dependent on oil , whose price seems set to remain high in the medium to long run .
Moving towards low-carbon and more energy efficient transport will depend on new technologies like hybrid and electrical motors , as well as developing more efficient urban and intermodal transport solutions as alternatives to road haulage .
Reaching these goals should be supported by financial , fiscal and pricing incentives .
Modal split intra-EU goods transport in 2010
Modal split intra-EU passenger transport in 2010
2 . Better mobility
The EU road transport sector has developed considerably over the years for the benefit of trade , the economy , the freedom of movement .
In some respects it has been too successful .
Traffic volumes are growing , but so is congestion .
To increase efficiency , the EU seeks to ensure well functioning open markets and uniform technical standards , and encourages the development of an integrated trans-European network and a better use of infrastructure by using intelligent transport systems .
Open markets
The EU internal market for transporting freight by road has been opened , and the EU has progressively established a comprehensive set of uniform rules to ensure fair competition among road transport operators .
The open internal market created the possibility for transport operators to supply services across national borders .
To do so , they must respect a set of common regulations .
Updated rules took effect in December 2011 .
To carry goods or passengers between Member States , operators must fulfill four criteria .
National authorities carry out regular checks to ensure that operators continue to satisfy the four criteria .
Operators fulfilling these criteria obtain a so-called community licence from their own Member State which allows them to carry out cross-border transport throughout the Union .
A certified copy of the community licence must be carried in each of their vehicles .
Drivers from non-EU countries must carry an attestation which certifies that they are legally employed by a licenced EU road haulage operator .
Good repute : professional operators must meet adequate ethical and entrepreneurial standards .
Failure to apply or respect EU rules will mean exclusion .
Sound financial standing : each year , operators need to show capital assets equivalent to € 9 000 for the first vehicle and € 5 000 for each additional vehicle .
Professional competence : operators must pass a standard exam to assess their practical knowledge and aptitude .
Establishment : operators must demonstrate that they have an effective and stable establishment in an EU Member State .
Cabotage is one way of reducing congestion and boosting efficiency .
Cabotage allows a haulier from one country to transport goods within another country on a temporary basis when making international deliveries .
Thus , if a Danish truck delivers a cargo to Bordeaux and is due to drive empty to pick up a return load in Lyon , it can carry goods from Bordeaux to Lyon .
Operators from all the Member States , including the ones which joined the EU in 2004 and 2007 , are now free to carry out temporary cabotage subject to certain rules .
EU road freight transport by type of operation in 2010
Common technical standards of vehicles
The EU sets limits for the weight and dimensions of heavy-duty vehicles used for transport in Europe to avoid damage to roads , bridges and other infrastructure , and to ensure the maximum level of safety on roads .
For instance , the maximum weight for trucks is 40 tonnes or 44 tonnes when carrying a container for combined transport operations .
The rules also allow Member States to authorise on their own territory and under certain conditions longer and heavier vehicles on their own roads or to carry out specific tasks .
A review of current rules has been announced .
The European Commission is due to submit proposals in 2013 to adapt maximum dimensions of vehicles , taking into account factors like better aerodynamic performance , fuel consumption , lower noise and emissions and the use of electric trucks with heavy batteries .
The Commission will also consider the evolution of containers , the potential of new technology , and the promotion of intermodality for freight transport .
The EU is also responsible for drawing up type-approval standards which enable the automotive industry to market vehicles in all Member States .
Towards an interoperable electronic tolling service
Compatible national electronic tolls systems , a legal requirement since 2007 , will help reduce delays and ease congestion .
The corresponding EU legislation provides for a European electronic toll service ( EETS ) whereby road users can subscribe to a single contract with one service provider and , using a single on-board unit , to pay tolls electronically throughout the EU .
By eliminating cash transactions at toll booths , traffic flows will improve and congestion will ease .
By the end of 2012 , regional cross-border electronic toll services will start to be available for trucks above 3.5 tonnes , buses and coaches as a first step towards EETS , and by the end of 2014 for all other vehicle categories .
Investing in the TEN-T network
To support the development of the TEN-T , the EU finances projects that help to remove bottlenecks and to build the missing links thanks to its so-called structural Funds and the newly created Connecting Europe Facility .
The projects eligible are identified in the guidelines for the development of the TEN-T .
In road transport , priorities are to deploy Intelligent Transport systems ( ITS ) like traffic management information and route guidance , and parking areas .
The Commission has also proposed to support innovative freight transport services that contribute to reduce carbon dioxide emissions .
Interoperability standards for intelligent transport systems
Technology will help the sector meet the challenges both in terms of more efficient use of infrastructures and transport management and a smaller carbon footprint .
Smart logistics can , for instance , cut the number of empty journeys made by trucks which still account for nearly 25 % of the total .
Galileo , the European satellite navigation system , and other navigational aids will reduce journey times , provide real-time information to ease congestion and offer track-and-trace monitoring for vehicles and cargos , also helping preventing cargo theft .
In addition to providing possible financial support through the TEN-T or research programmes , the EU had adopted in 2010 a Directive to progressively define at European level common interoperability standards for a number of ITS services such as : reservation services for safe and secure parking places for trucks and commercial vehicles , interoperable EU-wide eCall to bring rapid assistance to motorists involved in a collision , thus increasing road safety , real-time traffic and multimodal travel information services .
3 . Making road transport safer for citizens , passengers and transport workers
Passenger rights
The EU adopted a new regulation in February 2011 setting for the first time a series of rights for passengers traveling long distance by coach or bus .
This brings road transport in line with rail , air and sea travel where passengers already enjoy a series of rights .
The Regulation will apply from March 2013 .
Subject to certain exceptions , this Regulation applies to passengers traveling with regular services where either the boarding or the arriving point is within the European Union ( EU ) and where the scheduled distance of the service is 250 km or more .
The Regulation covers non-discrimination between passengers regarding transport conditions offered by carriers , rights of passengers in the event of accidents , non-discrimination and assistance for disabled persons and persons with reduced mobility , rights of passengers in case of cancellation or delay , minimum information to be provided to passengers , the handling of complaints and general rules on enforcement .
Safe transport
Better mobility also means safer mobility .
Although road transport is a lot safer than it was , it is still considerably more dangerous than other means of travel .
A total of 30 300 people died in road accidents in 2011 , which is more than 46 % down on 2000 .
The EU 's aim is to halve this number by 2020 , using technology ( real-time traffic information , advanced braking systems , automatic driver assistance , better vehicle-to-infrastructure communications , driver behaviour control systems , etc . ) , enforcement , and education with a particular focus on vulnerable road users .
Although accidents involving trucks or coaches represent only a small proportion , they are more severe . ( 14 % of road fatalities result from accidents involving heavy duty vehicles ) .
Driver fatigue and users ' behaviour on the road are the main cause or an aggravating factor in one out of three of these accidents .
Maximum driving time
The EU sets a series of social provisions for professional drivers ' working conditions , which also contribute to road safety and underpin fair competition among operators .
The rules fix :
a daily driving period of no more than nine hours , with a break of at least 45 minutes after 4.5 hours of driving .
Ten-hour driving periods are possible twice a week ;
a maximum weekly driving time of 56 hours ( or 90 hours per fortnight ) ;
daily rest periods of at least 11 hours ( with the option of cutting this to nine hours , three times per week ) ;
a weekly rest period of 45 continuous hours ; this can be cut to 24 hours every second week ( with appropriate compensation ) .
Compliance with these provisions is subject to continuous monitoring and controls by Member States .
These consist of spot checks of vehicle tachograph records at the roadside or at the premises of vehicle operators .
Training of drivers
The EU promotes appropriate training for drivers .
Under a 2003 directive , they must undergo formal vocational training .
Until then , most drivers gained experience ‘ on the job ’ which allowed them to move on to larger categories of vehicles .
Now the EU insists that formal training is the only way to ensure that drivers have the requisite up-to-date skills and knowledge .
The directive imposes on drivers 35 hours of periodic training every five years .
Drivers learn about safe and eco-friendly driving , vehicle loading , and passenger comfort .
A smart digital tachograph
Every lorry , bus and coach on EU roads must be fitted with a tachograph to record information on each journey such as driving times and rest periods .
Tachographs were first introduced in 1985 .
Now digital , they are installed on board six million trucks and buses and can record data such as speed , distance covered and driver identification — with much greater accuracy .
These data once processed make it easier to check compliance with EU rules by operators and drivers .
The next generation of tachograph will include satellite positioning and road communication systems .
Furthermore , the operators will be able to connect this new tachograph to their own on-board computer .
Working time of drivers
Since 2005 , drivers must respect an average maximum working time of 48 hours per week ( averaged out over a four-month period ) , while night work cannot exceed 10 hours in any 24-hour period .
No more than six hours can be worked consecutively without a break of at least 30 minutes .
See and be seen
Dedicated daytime running lights ( DRLs ) are the lamps on vehicles that switch on automatically when the engine is started .
They substantially increase the visibility of motor vehicles .
In contrast to conventional headlights , DRLs do not help the driver see the road but rather help other road users see the approaching vehicle .
DRLs ( already in use in most EU countries ) became mandatory from 2011 for all new cars and light commercial vehicles in the EU .
Trucks and buses were to follow in August 2012 .
Tight controls on dangerous goods
Moving dangerous goods like chemicals or flammable materials by road ( and other means of transport ) is governed by a series of EU directives .
The rules also cover the movement of transportable pressure equipment .
They prescribe requirements like technical type-approval for vehicles , special training for drivers and uniform control procedures for checking the transport of dangerous goods both at the roadside and at operators ' premises .
EU laws also provide for the appointment and training of safety advisers .
Removing blind spots
Any truck driver knows that big vehicles have a blind spot when they turn right ( or left in the case of right-hand-drives ) .
The European Commission estimated that the problem was causing about 400 road deaths a year in Europe , with cyclists being particularly vulnerable .
Since 2009 ( 2007 for new vehicles ) all trucks above 3.5 tonnes are fitted with upgraded wide-angle rear-view mirrors which reduce the blind spot by at least 85 % .
Safe and secure parking areas
Safety is about safe parking as well as safe driving .
As transport volumes have risen , so has crime in the form of cargo theft and attacks on drivers .
The EU has co-funded a number of pilot projects for safe and secure parking areas .
Two were in Germany ( Wörnitz and Uhrsleben ) , one in northern France ( Valenciennes ) , one in southern England ( Ashford ) and one in Estonia ( Narva ) at the border with the Russian Federation .
The EU has also issued a handbook to help truck park operators develop their sites in line with the required security and quality standards ( http : / / ec.europa.eu / transport / road / parking / doc / handbook _ for _ labelling.pdf ) .
This is to provide adequate and secure rest facilities for drivers , and an electronic parking and queue reservation system to ease congestion and reduce environmental harm .
The new TEN-T guidelines proposed by the Commission require Member States to provide parking areas at 50 km intervals on motorways to offer commercial road users adequate parking space with an appropriate level of safety and security .
4 . Cleaner transport
Making transport cleaner requires investment in innovation technologies and promotion of supporting standards .
EU laws already make possible that transport users contribute to the cost of Europe 's road infrastructure via charges linked to the dimensions of their vehicles and the distances travelled .
Now the road transport sector is under growing expectations that it would pay for the environmental damage it causes in terms of greenhouse gas emissions and noise .
Road transport accounts for about 18 % of all EU emissions .
New engines and new fuels
Technology and innovation are making road transport greener .
R &amp; D helps to produce more energy-efficient engines which consume less fuel and therefore produce lower exhaust emissions .
The so-called Euro-VI standard for engines , to be introduced in 2013 and mandatory as from 2014 , will reduce emissions by more than 60 % .
Cleaner fuels are also being developed , with electricity being the preferred new power source for short distances , methane and hydrogen for middle distances , and liquefied gas ( LNG and LPG ) for longer journeys .
The EU finances R &amp; D programmes and works on standards on vehicles and infrastructures to facilitate the development of electric transport ( like accessible charging points ) .
The shift towards new hybrid and electric propulsion systems is a huge challenge for the future that can only be met through a coordinated approach by the automotive industry at European level .
It is important for European industry to be at the cutting-edge in those technologies which can improve its share of international markets .
City logistics
New means have been found to increase the sustainability and the efficiency of urban goods delivery .
With urban electric freight vehicles , big trucks no longer need to enter city centres , reducing air and noise pollution .
Electric vehicles improve air quality through lower emissions , and more silent vehicles mean less congestion since they can make deliveries at night without disturbing local residents .
The EU is financing pilot projects in this domain and promotes the exchange of good practice .
Intermodal transport
Its flexibility , speed and ability to deliver door-to-door make road transport virtually unbeatable over distances of up to 300 km .
This is unlikely to change .
Technology , research and innovation , and better logistics will make road transport on short routes even more efficient and more environment-friendly .
For longer distances , there is a strong environmental case for intermodal transport where the main part of the route is by rail , sea or inland waterway with a short road journey at one or both ends .
Successful examples of getting freight off the roads are , for instance , motorways of the sea where lorries or their trailers make part of their north-south or east-west journey across Europe by specialised ferries .
Another intermodal match is taking shape for north-south road freight over the Alps where lorries will cross the mountains through tunnels on specially constructed trains and railcars .
Paying the right price
Because they do not reflect the actual cost of using transport infrastructure , current taxes and charges do not send the right price signals to operators .
The EU is trying to restructure these into a fairer and more efficient road charging system .
The present EU road charging system is based on the so-called Eurovignette directive .
First adopted in 1999 , it allowed Member States to levy a charge on heavy goods vehicles using trunk roads so as to pay for the maintenance and repair of the road infrastructure .
An updated Eurovignette directive adopted in 2011 provides for an increased charge to take account of exhaust emissions and noise pollution as well .
It applies the polluter-pays principle whereby cleaner trucks will pay less than those with higher exhaust emissions .
The new legislation also authorises national governments to charge higher tolls at peak times .
It allows an extra toll charge in mountain areas under certain conditions , principally that the revenue is invested to construct alternative routes .
This EU legislation does not apply to private cars or light commercial vehicles below 3.5 tonnes .
But the European Commission monitors charging schemes where they are applied by individual Member States to ensure that they are proportionate and do not discriminate on grounds of the user 's nationality or country of residence .
Vehicle tax and fuel duties
The EU has set common rules for annual taxes for heavy goods vehicles over 12 tonnes , which goes some way to reduce differences in national taxation levels .
This includes minimum rates based on the category and the size of the vehicle .
But Member States fix the structure of the taxes and procedures for collecting them .
An EU directive has harmonised the structure of fuel taxes ( excise duties ) across the EU with a minimum level for diesel fuel ( by far the most widely used in road transport ) of € 330 per 1 000 litres .
Share of VAT and of excise duties in each countries
Average diesel price per litre on 05 / 03 / 2012 by EU country
5 . The EU 's international reach
EU road transport policy does not stop at the external borders of the EU .
The EU and its neighbours in the European Free Trade Association apply one set of rules .
Another set of rules applies for EU relations with its other neighbours mainly in eastern Europe and the western Balkans .
Among the EFTA countries , there are again two groups : the EEA countries ( Iceland , Liechtenstein and Norway ) on the one hand , and Switzerland on the other .
The EEA countries participate fully in the internal market , Switzerland concluded a bilateral Land Transport Agreement with the EU in 1999 .
It regulates the conditions under which Switzerland has access to the internal road transport market of the EU and vice-versa .
Switzerland has committed itself to and made considerable progress in constructing two major rail base tunnels in the Alps , which should help promote a modal shift to rail and thus reduce road congestion and air and noise pollution along the major transalpine corridors .
Through its international cooperation , the EU also promotes its social rules in other European neighbouring countries , plus the Caucasus and Central Asia in the framework of the European agreement Concerning the Work of Crews of Vehicles Engaged in International Road Transport ( AETR ) .
The EU Member States are among the 50 Contracting Parties to the AETR .
The AETR has aligned its provisions for driving time , breaks and rest periods with current EU legislation .
AETR countries have also introduced the digital tachograph which became compulsory in 2010 on vehicles involved in international transport .
Moreover , the Interbus agreement on international occasional carriage of passengers by coach and bus facilitates passenger transport services between the EU and a number of third countries .
REFERENCES
GENERAL TRANSPORT POLICY
White paper on transport
Roadmap to a single European transport area – towards a competitive and resource-efficient transport system , COM ( 2011 ) 144 final
ACCESS TO ROAD TRANSPORT MARKET
Admission to the occupation
Regulation ( EC ) No 1071 / 2009 of 21 October 2009 establishing common rules concerning the conditions to be complied with to pursue the occupation of road transport operator and repealing Council Directive 96 / 26 / EC
Road haulage
Regulation ( EC ) No 1072 / 2009 of 21 October 2009 on common rules for access to the international road haulage
Directive 2006 / 1 / EC of 18 January 2006 on the use of vehicles hired without drivers for the carriage of goods by road
Passenger transport
Regulation ( EC ) No 1073 / 2009 of 21 October 2009 on common rules for access to the international market for coach and bus services , and amending Regulation ( EC ) No 561 / 2006
Regulation ( EC ) No 1370 / 2007 of 23 October 2007 on public passenger transport services by rail and by road and repealing Council Regulations ( EEC ) Nos 1191 / 69 and 1107 / 70
SAFETY AND SOCIAL ASPECTS
Driving time , working hours and rest periods
Directive 2002 / 15 / EC of 11 March 2002 on the organisation of the working time of persons performing mobile road transport activities
Standards and checks
Regulation ( EC ) No 561 / 2006 of 15 March 2006 on the harmonisation of certain social legislation relating to road transport and amending Council Regulations ( EEC ) No 3821 / 85 and ( EC ) No 2135 / 98 and repealing Council Regulation ( EEC ) No 3820 / 85
Council Regulations ( EEC ) No 3821 / 85 of 20 December 1985 on recording equipment in road transport
Directive 2006 / 22 / EC of 15 March 2006 on minimum conditions for the implementation of Council Regulations ( EEC ) No 3820 / 85 and ( EEC ) No 3821 / 85 concerning social legislation relating to road transport activities and repealing Council Directive 1988 / 599 / EEC
Directive 2003 / 59 / EC of 15 July 2003 on the initial qualification and periodic training of drivers of certain road vehicles for the carriage of goods or passengers , amending Council Regulation ( EEC ) No 3820 / 85 and Council Directive 91 / 439 / EEC and repealing Council Directive 76 / 914 / EEC
Passenger rights
Regulation ( EU ) No 181 / 2011 of the European Parliament and of the Council of 16 February 2011 concerning the rights of passengers in bus and coach transport and amending Regulation ( EC ) no 2006 / 2004
TAXES AND CHARGES
Eurovignette and tolls
Communication from the Commission on a strategy for the internalisation of external costs , COM ( 2008 ) 435 final
Directive 1999 / 62 / EC on the charging of heavy goods vehicles for the use of certain infrastructures as amended by Directives 2006 / 38 / EC and 2011 / 76 / EU
Directive 2004 / 52 / EC of 29 April 2004 on the interoperability of electronic road toll systems in the Community
Fuel excise duty
Directive 2003 / 96 / EC of 27 October 2003 restructuring the Community framework for the taxation of energy products and electricity
TECHNICAL ASPECTS
Weight and dimensions
Directive 96 / 53 / EC of 25 July 1996 laying down for certain road vehicles circulating within the Community the maximum authorized dimensions in national and international traffic and the maximum authorized weights in international traffic
Directive 97 / 27 / EC of 22 July 1997 relating to the masses and dimensions of certain categories of motor vehicles and their trailers and amending Directive 70 / 156 / EEC
Intelligent transport systems
Directive 2010 / 40 / EU of 7 July 2010 on the framework for the deployment of intelligent transport systems in the field of road transport and for interfaces with other modes of transport
Dangerous goods
Directive 2008 / 68 / EC of 24 September 2008 on the inland transport of dangerous goods
INFRASTRUCTURE
Council resolution of 8 November 2010 on preventing and combating road freight crime and providing secure truck parks
Directive 2008 / 96 / EC of 19 November 2008 on road infrastructure safety management
Directive 2004 / 54 / EC of 29 April 2004 on minimum safety requirements for tunnels in the Trans-European Road Network
Decision 661 / 2010 / EU of the European Parliament and of the Council of 7 July 2010 on guidelines for the development of the trans-European transport network
AGREEMENT WITH NON-MEMBER COUNTRIES
EC-Swiss Confederation agreement
Agreement between the European Community and the Swiss Confederation on the carriage of goods and passengers by rail and road ( effective 1 June 2002 )
Interbus agreement
Council Decision 2002 / 917 / EC of 3 October 2002 on the conclusion of the Interbus Agreement on the international occasional carriage of passengers by coach and bus
AETR agreement
European Agreement Concerning the Work of Crews of Vehicles Engaged in International Road Transport ( AETR ) ( Effective 1 July 1970 )
HOW TO OBTAIN EU PUBLICATIONS
Free publications :
via EU Bookshop ( http : / / bookshop.europa.eu ) ;
at the European Union 's representations or delegations .
You can obtain their contact details on the Internet ( http : / / ec.europa.eu ) or by sending a fax to + 352 2929-42758 .
Priced publications :
via EU Bookshop ( http : / / bookshop.europa.eu ) .
Priced subscriptions ( e.g. annual series of the Official Journal of the European Union and reports of cases before the Court of Justice of the European Union ) :
via one of the sales agents of the Publications Office of the European Union ( http : / / publications.europa.eu / others / agents / index _ en.htm ) .
European Commission
Road Transport - A change of gear
MI3112464ENE
